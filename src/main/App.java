package main;

import controller.TicTacToe;

/**
 * Classe principale utilisée pour lancer l'application 
 * The main class use to launch the application
 */
public class App {

	public static void main(String[] args) {

		// Création d'une nouvel objet du controleur et lancement du jeu
		// Create a new object of the controler and launch the game
		TicTacToe control = new TicTacToe();
		control.lancerJeu();
		
	}

}
