package controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import model.Player;
import view.UserView;

/**
 * Controlleur qui gère la logique du jeu TicTacToe 
 * Controller class to put the logic behind the game
 */
public class TicTacToe {
	/**
	 * Attributes
	 */
	// Grille avec les numéro de chaque position disponible
	private char[][] explainGameBoard = {	{'1', '2', '3'},
			{'4', '5', '6'},
			{'7', '8',  '9'}};
	// Grille du jeu qui est vide au départ
	private char[][] gameBoard = {	{' ',' ',' '},
			{' ', ' ', ' '},
			{' ',' ', ' '}};
	// Type de joueur MP = Main Player (ici l'utilisateur de l'application)
	private String TYPE_MP = "MP";
	// Type de joueur CPU = L'ordinateur (ici choix aléatoire)
	private String TYPE_CPU = "CPU";
	
	// Les 2 joueurs du jeu
	private Player mainPlayer = new Player("user1", TYPE_MP);
	private Player cpu = new Player("cpu1", TYPE_CPU);
	
	// La liste des placements choisi pour chaque pièce jouées
	private ArrayList<Integer> allPositions =  new ArrayList<Integer>();



	/**
	 * Constructeur de la class TicTacToe
	 */
	public TicTacToe() {
		super();
	}

	/**
	 * Méthode qui permet de lancer le jeu et de l'afficher
	 */
	public void lancerJeu() {
		UserView.printWelcomeMessage();
		UserView.printGameBoard(this.getExplainGameBoard());

		// Boucle qui permet au joueurs de jouer et qui s'arrêter à la fin du jeu
		while(true) {
			// Placement d'une pièce par le joueur
			placeAPiece(mainPlayer);
			UserView.printGameBoard(this.getGameBoard());
			if(isFinalMove(mainPlayer)) {
				break;
			}

			// Placement d'une pière par le 
			placeAPiece(cpu);
			UserView.printGameBoard(this.getGameBoard());
			if(isFinalMove(cpu)) {
				break;
			}
		}
	}

	/**
	 * Permet à un joueur d'effectuer sa prochaine action en fonction des cases disponibles
	 * @param player
	 */
	public void placeAPiece(Player player) {
		int pos = 0;
		
		// Si le joeur est le de type TYPE_MP
		if(player.getType() == TYPE_MP) {
			// Demander un placement tant que ce placement n'est pas un déjà joué
			pos = UserView.askNextPositionPlayer(false);
			while(this.getAllPositions().contains(pos) || player.getPositions().contains(pos)) {
				pos = UserView.askNextPositionPlayer(true);
			}
			mainPlayer.addPosition(pos);
			this.addPosition(pos);
		}
		// Si le joeur est le de type TYPE_CPU
		else if(player.getType() == TYPE_CPU) {
			// Générer un placement tant que ce placement n'est pas un déjà joué
			pos = getNextPositionCPU();
			while(getAllPositions().contains(pos) || player.getPositions().contains(pos)) {
				pos = getNextPositionCPU();
			}
			cpu.addPosition(pos);
			this.addPosition(pos);
		}

		// Ajouter une pièce sur
		addPieceOnBoard(pos, player);
	}

	/**
	 * Définir si la dernière action d'un joeur met fin à la partie
	 * @param player 
	 * @return True, c'est la fin de la partie ou False, si le jeu continue.
	 */
	public boolean isFinalMove(Player player) {
		boolean res = false;

		// Si grille complete alors annoncer que le jeux est terminé
		if(getAllPositions().size() > 8) {
			UserView.endGameMessage();
			res = true;
		}

		// Si la dernière action du joueur est une action gagnante affichier qu'il a gagné
		if(isWinner(player.getPositions())) {
			if(player.getType() == TYPE_MP) {
				UserView.mainPlayerWinMessage();
			}
			else {
				UserView.cpuWinMessage();
			}
			res = true;
		}


		// Si grille complete sans gagnant alors annoncer qu'il n y a pas de gagnant
		if(getAllPositions().size() > 8) {
			UserView.endGameMessage();
			res = true;
		}

		return res;
	}



	/**
	 * Ajout d'une pièce dans la grille de jeu par un joueur en fonction d'un placement donné
	 * @param pos = placement de la pièce
	 * @param player = joueur 
	 */
	public void addPieceOnBoard(Integer pos, Player player) {

		char symbol = ' ';

		// Définit si le symbole en fonction dy type de joueur
		if(player.getType() == TYPE_MP) {
			symbol = 'X';
		}else if(player.getType() == TYPE_CPU) {
			symbol = 'O';
		}

		// Place le symbole selon le placement sélectionné dans la position correcpondante dans le tableau de la grille du jeu
		switch(pos) {
		case 1:
			this.gameBoard[0][0] = symbol;
			break;
		case 2:
			this.gameBoard[0][1] = symbol;
			break;
		case 3:
			this.gameBoard[0][2] = symbol;
			break;
		case 4:
			this.gameBoard[1][0] = symbol;
			break;
		case 5:
			this.gameBoard[1][1] = symbol;
			break;
		case 6:
			this.gameBoard[1][2] = symbol;
			break;
		case 7:
			this.gameBoard[2][0] = symbol;
			break;
		case 8:
			this.gameBoard[2][1] = symbol;
			break;
		case 9:
			this.gameBoard[2][2] = symbol;
			break;
		default :
			break;
		}

	}

	/**
	 * Vérifier si les positions données en paramètre correspondent à un placement de pièces gagnant
	 * @param positions joué par un joueur
	 * @return Vrai ou faux 
	 */
	public static boolean isWinner(ArrayList<Integer> positions) {

		// Création de plusieurs liste qui correspondent à des composition de placements gagnant
		List<Integer> topRow = Arrays.asList(1, 2, 3);
		List<Integer> midRow = Arrays.asList(4, 5, 6);
		List<Integer> botRow = Arrays.asList(7, 8, 9);

		List<Integer> leftCol = Arrays.asList(1, 4, 7);
		List<Integer> midCol = Arrays.asList(2, 5, 8);
		List<Integer> rightCol = Arrays.asList(3, 6, 9);

		List<Integer> cross1 = Arrays.asList(1, 5, 9);
		List<Integer> cross2 = Arrays.asList(7, 5, 3);

		// Création d'une liste de liste de placements gagnant
		List<List<Integer>> winningPositions = new ArrayList<List<Integer>>();
		winningPositions.add(topRow);
		winningPositions.add(midRow);
		winningPositions.add(botRow);

		winningPositions.add(leftCol);
		winningPositions.add(midCol);
		winningPositions.add(rightCol);

		winningPositions.add(cross1);
		winningPositions.add(cross2);

		// Boucler sur la liste des placements gagnant pour vérifier si les placement du joeur contiennent une composition gagnante
		for(List<Integer> l : winningPositions) {
			if(positions.containsAll(l)) {
				return true;
			}
		}

		// Si aucun placement de pièce ne correspond à une placement gagnant, retourner faux
		return false;
	}

	/**
	 * Permet de selectionner de manière aléatoire un chiffre de 1 à 9 qui correspondera à la prochaine position sur la grille pour le cpu
	 * @return Retourne la position selectionné pour le cpu
	 */
	public int getNextPositionCPU() {
		Random rand = new Random();
		int cpuPos = rand.nextInt(9)+ 1;
		UserView.pickPositionMessage(cpuPos);
		return cpuPos;
	}

	/**
	 * Méthode qui rajoute une position jouée à la liste des position déjà joué
	 * @param pos = Une position jouée 
	 */
	public void addPosition(int pos) {
		this.allPositions.add(pos);
	}

	/**
	 * Méthode qui retourne la liste des position sous forme de liste d'entier
	 * @return ArrayList<Integer>
	 */
	public ArrayList<Integer> getAllPositions() {
		return this.allPositions;
	}

	/**
	 * Méthode qui retourne la grille expliquée sous forme de tableau de charatère à 2 dimension
	 * @return char[][]
	 */
	public char[][] getExplainGameBoard() {
		return explainGameBoard;
	}

	/**
	 * Méthode qui retourne la grille sous forme de tableau de charatère à 2 dimension
	 * @return char[][]
	 */
	public char[][] getGameBoard() {
		return gameBoard;
	}

}
