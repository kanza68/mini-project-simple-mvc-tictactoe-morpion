package view;

import java.util.Scanner;

/**
 * View class to interact with the user
 */
public class UserView {

	/**
	 * Fonction qui affiche un message de bienvenue à l'utilisateur
	 */
	public static void printWelcomeMessage() {
		System.out.println("*** Bienvenue dans ce jeu de TicTacToe");
		System.out.println("Veuillez vous référer à la grille exemple pour connaitre le numéro à utiliser pour chaque case");
	}
	
	/**
	 * Affiche une grille de jeux sur la console
	 * @param gameBoard
	 */
	public static void printGameBoard(char[][] gameBoard) {
		for(char[] row : gameBoard) {
			for(char c : row) {
				System.out.print(c + "|");
			}
			System.out.println();
			System.out.println("-+-+-+-");
		}
	}
	
	/**
	 * Demande au joueur principal de saisir un placement pour la prochaine pièce à jouer
	 * @param err = si il y a eu erreur lors de la dernière saisie
	 * @return le placement saisie pour le joueur
	 */
	@SuppressWarnings("resource")
	public static int askNextPositionPlayer(boolean err) {
		Scanner scan = null;
		scan = new Scanner(System.in);
		if(err) {
			System.out.println("Position taken!");
		}
		System.out.println("---------------------------");
		System.out.println("Enter your placement (1-9) : ");
		int playerPos = scan.nextInt();
		
		System.out.println("---------------------------");
		System.out.println("Choose position : " + playerPos);
		System.out.println("---------------------------");
		

		return playerPos;
	}

	/**
	 * Affiche le placement sélectionné pour le CPU
	 * @param position
	 */
	public static void pickPositionMessage(int position) {
		System.out.println("---------------------------");
		System.out.println("Pick position by computer : " + position);
		System.out.println("---------------------------");
	}
	
	/**
	 * Affiche un message de fin de jeux
	 */
	public static void endGameMessage() {
		System.out.println("End of the game !");
	}

	/**
	 * Affiche un message gagnant pour le joueur principal
	 */
	public static void mainPlayerWinMessage() {
		System.out.println("You win !");
	}

	/**
	 * Affiche un message gagnant pour le CPU
	 */
	public static void cpuWinMessage() {
		System.out.println("Computer wins");
	}
	
	/**
	 * Affiche un message pour le cas où il n'y a aucun gagnant
	 */
	public static void noWinnerMessage() {
		System.out.println("No winner");
	}


}
