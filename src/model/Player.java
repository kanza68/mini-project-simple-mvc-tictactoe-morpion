package model;

import java.util.ArrayList;

public class Player {

	/**
	 * Attributes
	 */
	private ArrayList<Integer> positions;
	private String name;
	private String type;
	
	/**
	 * Constructor of the Player class
	 * @param name
	 * @param type
	 */
	public Player(String name, String type) {
		super();
		this.name = name;
		this.type = type;
		this.positions = new ArrayList<Integer>();
	}
	
	/**
	 * 
	 * @return
	 */
	public ArrayList<Integer> getPositions() {
		return positions;
	}
	
	/**
	 * 
	 * @param positions
	 */
	public void setPositions(ArrayList<Integer> positions) {
		this.positions = positions;
	}
	
	/**
	 * Add a position in the list of positions of the player
	 * @param pos
	 */
	public void addPosition(int pos) {
		this.positions.add(pos);
	}
	
	/**
	 * 
	 * @return
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * 
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getType() {
		return type;
	}
	
	/**
	 * 
	 * @param type
	 */
	public void setType(String type) {
		this.type = type;
	}
	
}
