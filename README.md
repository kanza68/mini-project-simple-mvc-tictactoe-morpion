# Mini project - Simple MVC Tictactoe-Morpion


## (ENG)
## About the project
This is a simple and small project with an MVC pattern.
It enables you to play a TicTacToe game against a CPU(computer player) on a terminal.

The computer player, make random automatic selection.

## Goal of this project
This project is part of my objective to maintain and improve my skills for 2024.



## (FR)
## Introduction
Il s'agit d'un petit projet simple avec un modèle MVC.
Il vous permet de jouer à au Morpion contre un CPU (joueur) sur un terminal.

Le joueur automatique effectue une sélection automatique et aléatoire de ses placements.

## Objectif de ce projet
Ce projet fait partie de mon objectif de maintien et d'amélioration de mes compétence pour cette année 2024.
